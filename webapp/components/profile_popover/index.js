const {connect} = window['react-redux'];
const {bindActionCreators} = window.redux;
import {dialUsername} from '../../actions/actions';

import {getCurrentUser} from 'mattermost-redux/selectors/entities/users';
import ProfilePopover from './profile_popover.jsx';

function mapStateToProps(state, ownProps) {
    const {username} = getCurrentUser(state);
    return {
        username,
        ...ownProps
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({
            dialUsername
        }, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePopover);
