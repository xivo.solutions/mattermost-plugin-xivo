import Client from '../client';

export function dialUsername(fromUsername, toUsername) {
    console.log(`dialUsername from ${fromUsername} to ${toUsername}`);
    return async (dispatch, getState) => {
        try {
            const status = await Client.dialUsername(fromUsername, toUsername);
            return status;
        } catch (error) {
            throw error;
        }
    };
}
