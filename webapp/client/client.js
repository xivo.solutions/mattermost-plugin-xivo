import request from 'superagent';

/* Add web utilities for interacting with servers here */

export default class Client {
    constructor() {
        this.url = 'https://10.70.0.9/xuc/api/1.0/dialByUsername/avencall.xivo.solutions/';
    }

    dialUsername = async (fromUser, toUser) => {
        return this.doPost(`${this.url}${fromUser}/`, {username: toUser});
    }

    doPost = async (url, body, headers = {}) => {
        headers['X-Requested-With'] = 'XMLHttpRequest';

        try {
            const response = await request.
                post(url).
                send(body).
                set(headers).
                type('application/json').
                accept('application/json');

            return JSON.parse(response.status);
        } catch (err) {
            throw err;
        }
    }
}
